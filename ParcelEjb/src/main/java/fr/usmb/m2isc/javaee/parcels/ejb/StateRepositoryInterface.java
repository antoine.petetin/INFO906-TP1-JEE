package fr.usmb.m2isc.javaee.parcels.ejb;

import fr.usmb.m2isc.javaee.parcels.jpa.State;

public interface StateRepositoryInterface  extends RepositoryInterface<State>{
    State getByName(String name);
}
