<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="ISO-8859-1">
        <title>Display parcels</title>
        <link rel="stylesheet" type="text/css" href="css/base.css"/>
    </head>
    <body>
    <h1>Display parcels</h1>
    <p><a href="./parcel/create">Create a parcel</a></p>
    <table>
        <thead>
        <tr>
            <th>Id</th>
            <th>State</th>
            <th>Value</th>
            <th>Weight</th>
            <th>Origin</th>
            <th>Destination</th>
            <th>Current position</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${parcels}"  var="parcel">
            <tr>
                <td>${parcel.id }</td>
                <td>${parcel.state.toString()}</td>
                <td>${parcel.value}</td>
                <td>${parcel.weight}</td>
                <td>${parcel.origin.toString()}</td>
                <td>${parcel.destination.toString()}</td>
                <td>${parcel.position.toString()}</td>
                <td><a href="./parcel/update?id=${parcel.id}">Edit</a>  <a href="./?id=${parcel.id}">Show</a></td>

            </tr>
        </c:forEach>
        </tbody>
    </table>
    </body>
</html>