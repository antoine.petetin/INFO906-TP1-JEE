package fr.usmb.m2isc.javaee.parcels.ejb;

import fr.usmb.m2isc.javaee.parcels.jpa.Parcel;
import fr.usmb.m2isc.javaee.parcels.jpa.Place;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@Remote
public class ParcelRepository extends Repository<Parcel> implements ParcelRepositoryInterface {
	
	@PersistenceContext
	private EntityManager em;

	@EJB
	private PlaceRepositoryInterface placeRepository;
	@EJB
	private StateRepositoryInterface stateRepository;

	public ParcelRepository() {
		super(Parcel.class);

	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}



	@Override
	public int createParcel(double value, double weight, int originId, int destinationId) {
		Parcel parcel = new Parcel(weight,value);
		Place origin = placeRepository.get(originId);
		parcel.setState(stateRepository.getByName("Enregistré"));
		parcel.setOrigin(origin);
		parcel.setDestination(placeRepository.get(destinationId));
		parcel.setPosition(origin);
		em.persist(parcel);
		return parcel.getId();
	}


}
